unit DefaultProjection;

interface

type

  TProjection = class
    strict protected
      _coefficient : Double;
    public
      constructor Create(const Coefficient : Double);
      function ComputeProjection(const InpCoordinate : Double) : Double;

  end;

implementation

{ TProjection }

function TProjection.ComputeProjection(const InpCoordinate: Double): Double;
begin
  Exit(InpCoordinate * _coefficient);  
end;

constructor TProjection.Create(const Coefficient: Double);
begin
  _coefficient := Coefficient;
end;

end.
